/* Our Services Slider*/
// temporary data
let dataActive = null
// let dataActive = 'Web Design'
//start img view
let startImgView = 0
let endImgView = 12


const ourServiceListTitles = document.querySelector('.second_home-our-services-list');
const ourServiceTabsItems = document.querySelector('.services-list-items-tabs')
let activeTab
let activeTabCheck = document.querySelector('.second_home-our-services-list .active');
/* check default state*/
if (activeTabCheck === null) {
    activeTab = ourServiceListTitles.children[0].dataset.tabTitle
    Array.from(ourServiceTabsItems).forEach(item => {
        item.classList.remove('flex')
        item.classList.add('hide')
        console.log(Boolean(activeTabCheck));
    })
    ourServiceTabsItems.children[0].classList.add('flex')
}
else if (activeTabCheck) {
    activeTab = document.querySelector('.second_home-our-services-list .active').dataset.tabTitle;
    Array.from(ourServiceTabsItems.children).forEach(item => {
        item.classList.remove('flex')
        item.classList.add('hide')
    })
    ourServiceTabsItems.querySelector(`[data-tab-slide="${activeTab}"]`).classList.add('flex')
}
/* Event Listener*/
ourServiceListTitles.addEventListener('click', (e) => {
    let activeDataListTitles = e.target.dataset.tabTitle
    changeActiveTab(activeTab, activeDataListTitles)
})
function changeActiveTab(oldActiveTab, newActiveTab) {
    if (oldActiveTab === newActiveTab) {
        return
    }
    let oldTab = ourServiceListTitles.querySelector(`[data-tab-title="${oldActiveTab}"]`);
    let newTab = ourServiceListTitles.querySelector(`[data-tab-title="${newActiveTab}"]`);
    oldTab.classList.remove('active');
    newTab.classList.add('active')
    let oldTabItem = ourServiceTabsItems.querySelector(`[data-tab-slide="${oldActiveTab}"]`);
    let newTabItem = ourServiceTabsItems.querySelector(`[data-tab-slide="${newActiveTab}"]`);
    oldTabItem.classList.remove('flex');
    oldTabItem.classList.add('hide');
    newTabItem.classList.remove('hide');
    newTabItem.classList.add('flex');
    activeTab = newActiveTab
}

/* Our Amazing Work*/

const ourWorkListItles = document.querySelector('.third_home-our-work-list')
//collect dataparams from list
let dataParams = []
Array.from(document.querySelector('.third_home-our-work-list').children).forEach(item => {
    dataParams.push(item.getAttribute('data-work-tab'))
})

const ourWorkPortfolio = document.querySelector('.third_home-our-work-portfolio')
let ourWorkLoadMore = document.querySelector('.load-more')

// generate portfolio itemsto object
let portfolioItemsObject = {};
let imgIndex = 1
for (let index = 0; index < 49; index++) {
    const randomData = Math.floor(Math.random() * dataParams.length)
    portfolioItemsObject[`img${index}`] = `<a href="" class="portfolio-item" data-work-tab="${dataParams[randomData]}"><img class="portfolio-image" src="./img/graphic_design/graphic-design${imgIndex}.jpg" alt=""></a>`
    imgIndex++
    if (imgIndex > 12) {
        imgIndex = 1
    }
}
//function - view N images at portfolio
imgView(startImgView, endImgView)
function imgView(start, end) {

    for (let index = start; index < end; index++) {
        // console.log('eeeee');
        portfolioItem = portfolioItemsObject[`img${index}`]
        if (portfolioItem === undefined) {
            ourWorkLoadMore.classList.add('hide')
            break
        }
        ourWorkPortfolio.innerHTML += (portfolioItem);


    }
    filterPortfolioImg(dataActive)
    return startImgView = startImgView + 12, endImgView = endImgView + 12
}



/* Load More event*/
ourWorkLoadMore.addEventListener('click', () => {
    imgView(startImgView, endImgView)

})

ourWorkListItles.addEventListener('click', (e) => {
    // console.log(e.target.getAttribute('data-work-tab'));
    dataActive = e.target.getAttribute('data-work-tab')
    filterPortfolioImg(dataActive)
})

function filterPortfolioImg(dataActive) {
    let tempPortfolioItems = document.querySelectorAll('.portfolio-item');
    // console.log(tempPortfolioItems);
    tempPortfolioItems.forEach(item => {
        if ((item.getAttribute('data-work-tab') !== dataActive)&&(dataActive !== null)) {
            // console.log(dataActive);
                item.classList.add('hide')
                // console.log('hide', item);
            
        }

        else {
            item.classList.remove('hide')
            // console.log('show', item);
        }

    })
}

// filterPortfolioImg(dataActive)
